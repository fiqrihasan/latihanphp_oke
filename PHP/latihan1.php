<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $kalimat1 = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $kalimat2 = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        echo "<p> Kalimat 1: ".$kalimat1."</p>";
        echo "<p> Panjang karakter Kalimat 1: ".strlen($kalimat1)."</p>";
        echo "<p> Jumlah Kata pada kalimat 1: ".str_word_count($kalimat1)."</p>";
        echo "<br><br>";
         echo "<p> Kalimat 2: ".$kalimat2."</p>";
        echo "<p> Panjang karakter Kalimat 2: ".strlen($kalimat2)."</p>";
        echo "<p> Jumlah Kata pada kalimat 2: ".str_word_count($kalimat2)."</p>";
        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */
        $kalimat3 = "I love PHP";
        
        echo "<label>String: </label> \"$kalimat3\" <br>";
        echo "Kata pertama: " . substr($kalimat3, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: ".substr($kalimat3,2,5);
        echo "<br> Kata Ketiga: ".substr($kalimat3,7,10). "<br>" ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $kalimat4 = "PHP is old but sexy!";
        echo "String: \"$kalimat4\" "; 
        // OUTPUT : "PHP is old but awesome"
        echo "<p> kalimat 3: ".$kalimat4."</p>";
        echo"<p> kalimat 3 diganti: " .str_replace("sexy", "awesome",$kalimat4);

    ?>
</body>
</html>